package com.dao;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class Family {


    public Family(Human father, List<Human> children, Family family, Human mother, Set<Pet> pet) {
        this.father = father;
        this.children = children;
        this.family = family;
        this.pet = pet;
        this.mother = mother;
    }

    public Family(Human father, Human mother) {
        this.father = father;
        this.children = children;
        this.family = family;
        this.pet = pet;
        this.mother = mother;
    }


    public Family(Human father, List<Human> children, Human mother, Set<Pet> pet) {
        this.father = father;
        this.children = children;
        this.pet = pet;
        this.mother = mother;
    }


    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }


    private Human father;

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Set<Pet> getPet() {
        return pet;
    }

    public void setPet(Set<Pet> pet) {
        this.pet = pet;
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }
//
//    @Override
//    public String describePet() {
//        return super.describePet();
//    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    private List<Human> children;
    private Family family;
    Set<Pet> pet;
    private Human mother;

    //    @Override
//    public String greetPet() {
//        return "null";
//    }

    public boolean deleteChild(Human human) {
        List<Human> children2 = getChildren();

        boolean delete = false;
        getChildren().remove(human);
        if (children2.size() < getChildren().size()) {
            delete = true;
            return delete;
        }

        delete = false;
        return delete;
    }

    //
//
    public boolean deleteChild2(int index) {
        List<Human> children2 = getChildren();

        boolean delete = false;
        getChildren().remove(index);
        if (children2.size() > getChildren().size()) {
            delete = true;
            return delete;
        }

        delete = false;
        return delete;
    }

    //
//
    public void addChild(Human human) {
        getChildren().add(human);
    }


    public String countFamily() {
        int members = children.size() + 2;
        return "Count of members in the family:".concat(String.valueOf(members));
    }

    ;


    @Override
    public String toString() {
        return "Family{" +
                "father=" + getFather() +
                ", children=" + getChildren().toString() +
                ", family=" + getFamily() +
                ", pet=" + getPet() +
                ", mother=" + getMother() +
                '}';
    }
}
