package com.dao;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

public class Human {
    private String name;
    private String surname;

    public Human(String name, String surname, long birthDate, int iq) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    private long birthDate;
    private int iq;

    private Map schedule;

    public Human(String name, String surname, long birthDate, int iq, Map schedule) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;

        this.schedule = schedule;
    }

    public Human(String name, String surname, long birthDate) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;


    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }


    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public String describeAge() {
        Date date = new Date(getBirthDate() * 1000L);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT-4"));
        String javaDate = dateFormat.format(date);
        return javaDate;
    }

    public Map getSchedule() {
        return schedule;
    }

    public void setSchedule(Map schedule) {
        this.schedule = schedule;
    }

    ;


    public Human() {


    }

    public String describeAge(long date) {
        return "saa";
    }

    public String greetPet(Pet pet) {
        return "Hello, ".concat(pet.getNickname());
    }

    public String describePet(Pet pet) {
        String a = " ";
        if (pet.getTrickLevel() < 50) {
            a = "very sly";
        } else {
            a = "almost not sly";
        }

        System.out.println("I have a" + pet.getSpecies() + ", he is" + pet.getAge() + " years old, he is " + a);
        return a;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthdate=" + describeAge() +
                ", iq=" + iq +
                ", schedule=" + getSchedule() +
                '}';
    }


}
