package com.dao;

import java.util.List;
import java.util.Set;

public class Fish extends Pet implements Foul {


    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void setSpecies(Species species) {
        this.species = species;
    }

    Species species;

    @Override
    public void foul() {
        System.out.println("foul");
    }

    public Fish(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.FISH;
    }

    public Fish(String nickname) {
        super(nickname);
    }

    public Fish() {
    }

    @Override
    public void eat() {
        System.out.println("fish eating");
    }

    @Override
    public void respond() {
        System.out.println("fish responding");

    }

    @Override
    public String toString() {
        return "Fish{" +
                "species=" + species +
                "name=" + getNickname() +


                '}';
    }
}
