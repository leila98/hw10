package com.controller;


import com.dao.Family;
import com.dao.Human;
import com.dao.Pet;
import com.service.FamilyService;

import java.util.List;
import java.util.Set;

public class FamilyController {

    private final FamilyService familyService = new FamilyService();

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }


    public Family getFamilyByIndex(int index) {
        return familyService.getFamilyByIndex(index);
    }

    public void deleteFamily(int index) {
        familyService.deleteFamily(index);
    }

    public void deleteFamily(Family family) {
        familyService.deleteFamily(family);

    }

    public void saveFamily(Family family) {
        familyService.saveFamily(family);

    }


    public void displayAllFamilies() {
        for (Family family : familyService.getAllFamilies()) {
            System.out.println(family);
        }
    }


    public Family getFamiliesBiggerThan() {
        Family max = familyService.getFamilyByIndex(1);
        int membersMax = familyService.getFamilyByIndex(1).getChildren().size() + 2;

        for (Family family : familyService.getAllFamilies()) {
            Family min = family;
            int membersMin = family.getChildren().size() + 2;

            if (membersMin > membersMax) {
                max = min;
            }


        }
        return max;

    }


    public Family getFamiliesLessThan() {

        Family min = familyService.getFamilyByIndex(1);
        int membersMin = familyService.getFamilyByIndex(1).getChildren().size() + 2;

        for (Family family : familyService.getAllFamilies()) {
            Family max = family;
            int membersMax = family.getChildren().size() + 2;

            if (membersMax < membersMin) {
                min = max;
            }


        }
        return min;
    }


    public void countFamiliesWithMemberNumber() {

        int countOfFamilies = familyService.getAllFamilies().size();

        for (Family family : familyService.getAllFamilies()) {
            int countOfMembers = family.getChildren().size() + 2;
            System.out.println("countOfMembers for each Family:" + countOfMembers);

        }
        System.out.println("countOfFamilies for Family List:" + countOfFamilies);

    }

    public int count(Family family) {
        int countOfMembers = family.getChildren().size() + 2;
        return countOfMembers;
    }


    public void createNewFamily(Human human1, Human human2) {
        Family family = new Family(human1, human2);
        familyService.getAllFamilies().add(family);


    }

    public Family adoptChild(Family family, Human human2) {

        List<Human> children = family.getChildren();
        children.add(human2);

        return family;
    }

    public Family getFamilyById(int id) {
        return familyService.getAllFamilies().get(id);

    }

    public Set<Pet> getPets(int id) {

        return familyService.getAllFamilies().get(id).getPet();
    }


    public void addPet(int id, Pet pet) {
        Family family = familyService.getAllFamilies().get(id);
        Set<Pet> petSet = family.getPet();
        petSet.add(pet);

    }

    public void deleteFamilyByIndex(int index) {
        familyService.deleteFamily(index);
    }


}
