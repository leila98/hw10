package com;


import com.controller.FamilyController;
import com.dao.*;

import java.util.*;

public class Main {


    //    private final static FamilyController familyController = new FamilyController();
//
    public static void main(String[] args) {
        Human mother1 = new Human("khatira", "hasanova", 1458103130);
        Human father1 = new Human("arzu", "mammadov", 1458103130);
        Set<String> habits1 = new HashSet<String>();
        habits1.add("eat");
        habits1.add("walking");
        Dog dog = new Dog("toplan", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        RoboCat cat = new RoboCat("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };

        int[][] schedule1 = new int[7][1];
        Map<String, String> map1 = new HashMap<String, String>();
        map1.put(String.valueOf(DayOftheWeek.MONDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.TUESDAY), "Read a book");
        map1.put(String.valueOf(DayOftheWeek.WEDNESDAY), "Drink a coffee");
        map1.put(String.valueOf(DayOftheWeek.THURSDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.FRIDAY), "Meet with friends");
        map1.put(String.valueOf(DayOftheWeek.SUNDAY), "Buy furniture");
        map1.put(String.valueOf(DayOftheWeek.SATURDAY), "Buy the book");

        Human child1 = new Human("leila", "mammadova", 1458103130, 88);

        Human child2 = new Human("anar", "rzayev", 1458103130, 43);
        Human child3 = new Human("nargiz", "suleymanova", 1458103130, 43);

        List<Human> children = new ArrayList<>();

        Set<Pet> pets = new HashSet<>();
        pets.add(dog);


        Family family = new Family(father1, children, mother1, pets);
        Human mother2 = new Human("nazrin", "asadova", 1458103130);
        Human father2 = new Human("alikarimov", "mammadov", 1458103130);

        Human child4 = new Human("leila", "mammadova", 1458103130, 88);
        Human mother = new Human("khatira", "hasanova", 1458103130);
        System.out.println(mother);

        Human child7 = new Human("leila", "mammadova", 1998, 88);
        System.out.println(child7);


    }
}
